
 function fib(){
    document.getElementById("outputId").innerHTML = '';
     let fib = []; // will become finished list
     let upTo = document.getElementById("numFib").value; // input from user
     document.getElementById("numFib").value = '';
     console.log(upTo);
     for(let i = 0; i < upTo; i++){
         if(fib.length === 0){ // case 0
             fib[0] = 1;
         }else if(fib.length === 1){ // case 1
             fib[1] = 1;
         } else{ // rest
             fib[fib.length] = fib[fib.length-2] + fib[fib.length-1];
         }
         document.getElementById("outputId").innerHTML += '<li>' + fib[i].toString() + "</li>";
     }
 }

