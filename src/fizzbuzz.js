function fizzbuzz(){
    document.getElementById("outputFizzBuzz").innerHTML = '';
    let upTo = document.getElementById("numFizzBuzz").value;
    document.getElementById("numFizzBuzz").value = '';

    for(let i = 1; i <= upTo; i++){ // starting from 1 is a little easier
       let tmp = '';
        if(i % 3 === 0){
           tmp += 'Fizz';
        }
        if(i % 5 === 0){
           tmp += 'Buzz';
        }else if(tmp === ''){ // if emtpy. reduces a check here and there at least having else if
            tmp = '' + i;
        }
        document.getElementById("outputFizzBuzz").innerHTML += "<li>" +tmp + "</li>";
    }
}